<html>
<head>
    <title>This is my page</title>
</head>
<body>
<center><h1>Virtual Office</h1></center>
 <?php
$str = <<<MIRON
Example of string
spanning multiple lines
using heredoc syntax.
MIRON;

echo $str;

echo "<br>";

$arr22 = array("Name","Age","Address");
 
$arr2 = "I am not right";
 if(is_array($arr2)){
     echo "This is an array";
 } else {
     echo "No, this is not array";
}

echo "<br/>";
echo "<pre>";
print_r($arr22);
echo "</pre>";
echo "<br/>";
$y = serialize($arr22);
echo "<pre>";
print_r($y);
echo "</pre>";
// if we use 1 or 100000 or more the boolval will show 1, if we use 0 (zero) or false then this will give  0 (zero) value, that will not display anything
$var = 11;
$val = boolval($var);
echo $val;



?>

</body>
</html>