

<?php

//nl2br in addition to n, application, create line breaks
echo nl2br("Welcome\r\nThis is my HTML document", false);
echo "<br/>";
echo "<br/>";
echo "<br/>";

//nl2br application, create new
echo "Welcome\r\nThis is my HTML document";
echo "<br/>";
echo "<br/>";
echo "<br/>";
?>


<?php

//strlen application , shows how many charecters are there in the given line or words
$str = 'bangladesh';
echo strlen($str); // 6
echo "<br>";
$str = ' bangladesh';
echo strlen($str); // 7
echo "<br/>";
echo "<br/>";
echo "<br/>";
?>